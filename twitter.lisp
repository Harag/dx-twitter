(in-package :dx-twitter)

(defparameter *time-zone* -2)

(defun parse-trim-integer (string)
  (parse-integer (string-trim '(#\Space #\Tab #\Newline) string) :junk-allowed t))

(defun parse-twitter-created-at (date &key time-zone)
  (when date
    (let* ((split (dx-utils:split-string date #\Space))
           (split-time (dx-utils:split-string (fourth split) #\:)))
      
      (encode-universal-time 
       (parse-trim-integer (third split-time))
       (parse-trim-integer (second split-time))
       (parse-trim-integer (first split-time))
       (parse-trim-integer (third split))
       (dx-utils:month-number (second split)) 
       (parse-trim-integer (sixth split))
       (or time-zone *time-zone*)))))



(defun tw-request (app-id app-secret 
                   access-token access-token-secret
                   end-point 
                   &key method parameters 
                   content
                   content-type
                   signature-parameters
                   want-stream 
                   preserve-uri                 
                   (parse-json-p t)
                   (body-to-string-p t))
  :doc "Wraps drakma-request in a oauth aware request."
  (dx-oauth::oauth1-drakma-request 
   end-point 
   app-id 
   app-secret  
   :access-token access-token 
   :access-secret access-token-secret
   :method method :parameters parameters 
   :content content
   :content-type content-type
   :signature-parameters signature-parameters 
   :want-stream want-stream 
   :preserve-uri preserve-uri
   :body-to-string-p body-to-string-p
   :parse-json-p parse-json-p))

