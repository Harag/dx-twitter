(in-package :dx-twitter)

(defun at-least-1 (number)
  (if  number
      (if (stringp number)
          (parse-integer (string-trim '(#\Space #\Tab #\Newline) number) :junk-allowed t)
          number)
      1))

(defun twitter-authorize-url (request-token)
  (format nil "https://api.twitter.com/oauth/authorize?oauth_token=~A"
          request-token))

(defun request-request-token (app-id app-secret callback-uri)
  (let* ((end-point "https://api.twitter.com/oauth/request_token"))
    (oauth1-drakma-request
                  end-point
                  app-id app-secret
                  :callback-uri 
                  callback-uri
                  :method :post
                  :preserve-uri t)))

(defun request-access-token (app-id app-secret 
                             callback-uri 
                             oauth-token 
                             oauth-verifier 
                             request-token-secret)
  (let* ((end-point "https://api.twitter.com/oauth/access_token"))
    (oauth1-drakma-request
                  end-point
                  app-id app-secret 
                  :access-token oauth-token
                  :access-secret request-token-secret
                  :oauth-verifier oauth-verifier
                  :callback-uri 
                  callback-uri
                  :method :post
                  :preserve-uri t)))

(defun user-stream (app-id app-secret 
                    access-token access-token-secret
                    &key (parse-json-p t)
                    (body-to-string-p t))
  (let* ((end-point "https://userstream.twitter.com/1.1/user.json"))
    (tw-request 
     app-id app-secret   
     access-token access-token-secret  
     end-point
     :method :post
     :want-stream t 
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))

(defun home-timeline (app-id app-secret 
                      access-token access-token-secret
                      user-id
                      &key since-id max-id
                      (parse-json-p t)
                      (body-to-string-p t))
  (let* ((end-point (format nil "http://api.twitter.com/1.1/statuses/home_timeline.json?user_id=~A&count=800~A~A&include_rts=true&include_entities=true&contributor_details=true" 
                            user-id
                            (if max-id 
                                (format nil "&max_id=~A" (- max-id 1))
                                "")
                            (if since-id 
                                (format nil "&since_id=~A" since-id)
                                ""))))
    (tw-request
     app-id app-secret   
     access-token access-token-secret  
     end-point
     :method :get
     :signature-parameters 
     `(("contributor_details" "true")
       ("count" "800")
       ("include_entities" "true")
       ("include_rts" "true")
       ,@(if max-id
             `(("max_id" ,(format nil "~A" max-id))))
       ,@(if since-id
             `(("since_id" ,(format nil "~A" since-id))))
       ("user_id" ,(format nil "~A" user-id)))
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))

;;TODO: Implement this correctly;
(defun search-stream (app-id app-secret 
                      access-token access-token-secret
                      user-id
                      &key since-id
                      (parse-json-p t)
                      (body-to-string-p t))
  (let* ((end-point (format nil "http://api.twitter.com/1.1/statuses/home_timeline.json?user_id=~A&count=800&since_id=~A&include_rts=true&contributor_details=true" 
                            user-id
                             (at-least-1 since-id))))
    (tw-request
     app-id app-secret   
     access-token access-token-secret  
     end-point
     :method :get
     :signature-parameters
     `(("contributor_details" "true")
       ("count" "800")
       ("include_rts" "true")
       ("since_id"  ,(format nil "~A" (at-least-1 since-id)))
       ("user_id" ,(format nil "~A" user-id)))
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))

(defun mention-timeline (app-id app-secret 
                         access-token access-token-secret
                         user-id
                         &key since-id
                         (parse-json-p t)
                         (body-to-string-p t))
  (let* ((end-point (format nil "http://api.twitter.com/1.1/statuses/mentions_timeline.json?user_id=~A&count=800&since_id=~A&include_rts=true&include_entities=true&contributor_details=true" 
                            user-id
                            (if since-id
                                since-id
                                1))))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :get
     :signature-parameters 
     `(("contributor_details" "true")
       ("count" "800")
       ("include_entities" "true")
       ("include_rts" "true")
       ("since_id"  ,(format nil "~A" (at-least-1 since-id)))
       ("user_id" , (format nil "~A" user-id)))
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))

(defun direct-messages (app-id app-secret 
                         access-token access-token-secret
                        &key since-id
                        (parse-json-p t)
                        (body-to-string-p t))
  (let* ((end-point  
            (format nil "https://api.twitter.com/1.1/direct_messages.json?count=200&since_id=~A&include_entities=true"                             
                    (at-least-1 since-id))))
      (tw-request
       app-id app-secret 
       access-token access-token-secret
       end-point
       :method :get
       :signature-parameters
       `(("count" "200")
         ("include_entities" "true")
         ("since_id"  ,(format nil "~A" (at-least-1 since-id))))
       :parse-json-p parse-json-p
       :body-to-string-p body-to-string-p)))

(defun followers (app-id app-secret 
                          access-token access-token-secret
                          &key (parse-json-p t)
                          (body-to-string-p t))
  (let* ((end-point "https://api.twitter.com/1.1/followers/ids.json"))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :get
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))

(defun credentials (app-id app-secret 
                    access-token access-token-secret
                    &key (parse-json-p t)
                    (body-to-string-p t))
  (let* ((end-point "https://api.twitter.com/1.1/account/verify_credentials.json"))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :get
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))


(defun get-tweet (app-id app-secret 
                    access-token access-token-secret
                    tweet-id
                    &key (parse-json-p t)
                    (body-to-string-p t))
  (let* ((end-point  (format nil "http://api.twitter.com/1.1/statuses/show.json?id=~A" 
                             tweet-id)))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :get
     :signature-parameters `(("id" ,tweet-id))
      :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))

(defun tweet (app-id app-secret 
              access-token access-token-secret 
              message
              &key (parse-json-p t)
              (body-to-string-p t))
  (let* ((end-point
          "https://api.twitter.com/1.1/statuses/update.json")
         (status (dx-utils:trim-whitespace message)))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :post
     :content-type "application/x-www-form-urlencoded; charset=UTF-8"
     :content (dx-oauth:alist-to-url-encoded-string `(("status" . ,status))
                                       +utf-8+)
     :signature-parameters
     `(("status" ,status))
     :preserve-uri t
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))


(defun tweet-with-picture (app-id app-secret 
                           access-token access-token-secret 
                           message image-path
                           &key (parse-json-p t)
                           (body-to-string-p t))
  (let* ((end-point
          "https://api.twitter.com/1.1/statuses/update_with_media.json")
         (status (dx-utils:trim-whitespace message)))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :post
     :parameters `(("status" . ,status)
                   ("media[]" . ,(pathname image-path)))
     :preserve-uri t
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))


(defun favourite (app-id app-secret 
                          access-token access-token-secret 
                          tweet-id
                          &key (parse-json-p t)
                          (body-to-string-p t))
  (let* ((end-point
          "https://api.twitter.com/1.1/favorites/create.json"))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :post
     :signature-parameters
     `(("id" ,tweet-id))
     :preserve-uri t
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))


(defun retweet (app-id app-secret 
                access-token access-token-secret 
                tweet-id
                &key (parse-json-p t)
                (body-to-string-p t))
  (let* ((end-point
          (format nil
                  "https://api.twitter.com/1.1/statuses/retweet/~A.json"
                  tweet-id)))
    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :post
     ;;TODO: Check if this is not needed.
     ;;     :signature-parameters
     ;;     `(("id" . , tweet-id))
     :preserve-uri t
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))


(defun tweet-reply (app-id app-secret 
                    access-token access-token-secret  
                    message
                    at-user
                    in-reply-to-status-id
                    &key (parse-json-p t)
                    (body-to-string-p t))
  (let* ((end-point "https://api.twitter.com/1.1/statuses/update.json")
         (status (format nil "@~A ~A" at-user (dx-utils:trim-whitespace message))))

    (tw-request
     app-id app-secret 
     access-token access-token-secret
     end-point
     :method :post
    ;; :parameters `(("status" ,status)
    ;;               ("in_reply_to_status_id" ,in-reply-to-status-id)) 
     :content-type "application/x-www-form-urlencoded; charset=UTF-8"
     :content (alist-to-url-encoded-string `(("status" . ,status)
       ("in_reply_to_status_id" . ,in-reply-to-status-id))
                                       +utf-8+)

     :signature-parameters
     `(("status" ,status)
       ("in_reply_to_status_id" ,in-reply-to-status-id))   
     :preserve-uri t
     :parse-json-p parse-json-p
     :body-to-string-p body-to-string-p)))