(asdf:defsystem :dx-twitter
  :serial t
  :depends-on (:dx-oauth :dx-utils)
  :components ((:file "package")
               (:file "classes")
               (:file "twitter")
               (:file "end-points")
               (:file "parser")
               ))

