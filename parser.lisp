(in-package :dx-twitter)

(defgeneric tweet-exists (tweet))

(defmethod tweet-exists ((tweet list)))

(defgeneric parse-tweet (tweet raw-tweet))

(defmethod parse-contributors (raw-tweet)
  (let ((contributors))
    (dolist (contributor (dx-utils:get-json-value 
                          raw-tweet 
                          :contributors))
      (setf contributors (append contributors
                                 (list
                                  (make-instance 
                                   'contributor
                                   :id 
                                   (dx-utils:get-json-value 
                                    contributor 
                                    :id)
                                   :screen-name
                                   (dx-utils:get-json-value 
                                    contributor 
                                    :screen--name))))))
    contributors))

(defmethod parse-coordinates (coordinates)
  (when coordinates
    (let ((coords))
    
      (dolist (coord coordinates)
        (append coords
                (list
                 (make-instance 'coord
                                :longitude (first (dx-utils:get-json-value 
                                                   coord
                                                   :coordinates))
                                :latitude (second (dx-utils:get-json-value 
                                                   coord
                                                   :coordinates))))))
      (make-instance 'coordinate 
                     :coordinates coords
                     :type (dx-utils:get-json-value 
                            coordinates 
                            :coordinates
                            :type)))))

(defmethod parse-medias (raw-tweet)
  (let ((medias))
    (dolist (media (dx-utils:get-json-value 
                    raw-tweet 
                    :entities
                    :media))
      (setf medias 
            (append medias
                     (list
                     (make-instance 
                      'media-entity
                      :id (dx-utils:get-json-value media :id)
                      :media-url (dx-utils:get-json-value media :media-url)
                      :media-url-https (dx-utils:get-json-value media :media-url-https)
                      :url (dx-utils:get-json-value media :url)
                      :display-url (dx-utils:get-json-value media :display-url)
                      :expanded-url (dx-utils:get-json-value media :expanded-url)
                      :sizes 
                      (list
                       (make-instance 
                        'media-size
                        :size :large
                        :width (dx-utils:get-json-value media :sizes :large :w)
                        :height (dx-utils:get-json-value media :sizes :large :h)
                        :resize (dx-utils:get-json-value media :sizes :large :resize))
                       (make-instance 
                        'media-size
                        :size :medium
                        :width (dx-utils:get-json-value media :sizes :large :w)
                        :height (dx-utils:get-json-value media :sizes :large :h)
                        :resize (dx-utils:get-json-value media :sizes :large :resize))
                       (make-instance 
                        'media-size
                        :size :small
                        :width (dx-utils:get-json-value media :sizes :large :w)
                        :height (dx-utils:get-json-value media :sizes :large :h)
                        :resize (dx-utils:get-json-value media :sizes :large :resize))
                       (make-instance 
                        'media-size
                        :size :thumb
                        :width (dx-utils:get-json-value media :sizes :large :w)
                        :height (dx-utils:get-json-value media :sizes :large :h)
                        :resize (dx-utils:get-json-value media :sizes :large :resize)))
                      :type (dx-utils:get-json-value media :type)
                      :indices (dx-utils:get-json-value media :indices))))))
    medias))

(defmethod parse-urls (raw-tweet)
  (let ((urls))
    (dolist (url (or (dx-utils:get-json-value 
                      raw-tweet 
                      :entities
                      :urls)
                     (dx-utils:get-json-value 
                      raw-tweet 
                      :entities
                      :url
                      :urls)))
      (setf urls 
            (append urls
                    (list 
                     (make-instance 'url-entity
                                    :url (dx-utils:get-json-value url :url)
                                    :display-url (dx-utils:get-json-value url :display--url)
                                    :expanded-url 
                                    (dx-utils:get-json-value url :expanded--url)
                                    :indices (dx-utils:get-json-value url :indices))))))
    urls))

(defmethod parse-mentions (raw-tweet)
  (let ((mentions))
    (dolist (mention (dx-utils:get-json-value 
                    raw-tweet 
                    :entities
                    :mentions))
      (setf mentions 
            (append mentions
                   (list 
                    (make-instance 'mention-entity
                                   :id (dx-utils:get-json-value mention :id)
                                   :screen-name 
                                   (dx-utils:get-json-value mention :screen--name)
                                   :name (dx-utils:get-json-value mention :name)
                                   :indices (dx-utils:get-json-value mention :indices))))))
    mentions))

(defmethod parse-hashtags (raw-tweet)
  (let ((hashtags))
    (dolist (hashtag (dx-utils:get-json-value 
                    raw-tweet 
                    :entities
                    :hashtags))
      (setf hashtags 
            (append hashtags
                   (list 
                    (make-instance 'hashtag-entity
                                   :text (dx-utils:get-json-value hashtag :text)
                                   :indices (dx-utils:get-json-value hashtag :indices))))))
    hashtags))


(defmethod parse-entities (raw-tweet)
  (let ((media (parse-medias raw-tweet))
        (urls (parse-urls raw-tweet))
        (mentions (parse-mentions raw-tweet))
        (hashtags (parse-hashtags raw-tweet)))

    (append () 
            (if media
                (list media))
            (if urls
                (list urls))
            (if mentions
                (list mentions))
            (if hashtags
                (list hashtags)))))

(defmethod parse-user (raw-tweet)
  (when (dx-utils:get-json-value raw-tweet :user)
    (let ((user (make-instance 'twitter-user)))
      (setf (id user) 
            (dx-utils:get-json-value raw-tweet :user :id))
      (setf (contributers-enabled user) 
            (dx-utils:get-json-value raw-tweet :user :contributers--enabled))
      (setf (created-at user) 
            (parse-twitter-created-at 
             (dx-utils:get-json-value raw-tweet :user :created--at)))
      (setf (default-profile user) 
            (dx-utils:get-json-value raw-tweet :user :default--profile))
      (setf (default-profile-image user) 
            (dx-utils:get-json-value raw-tweet :user :default--profile--image))
      (setf (description user) 
            (dx-utils:get-json-value raw-tweet :user :description))
      (setf (user-entities user) 
            (parse-entities (dx-utils:get-json-value raw-tweet :user)))
      (setf (favourites-count user) 
            (dx-utils:get-json-value raw-tweet :user :favourites--count))
      (setf (follow-request-sent user) 
            (dx-utils:get-json-value raw-tweet :user :follow--request--sent))
      (setf (following user) 
            (dx-utils:get-json-value raw-tweet :user :following))
      (setf (followers-count user) 
            (dx-utils:get-json-value raw-tweet :user :followers--count))
      (setf (friends-count user) 
            (dx-utils:get-json-value raw-tweet :user :friends--count))
      (setf (geo-enabled user) 
            (dx-utils:get-json-value raw-tweet :user :geo--enabled))
      (setf (is-translator user) 
            (dx-utils:get-json-value raw-tweet :user :is--translator))
      (setf (lang user) 
            (dx-utils:get-json-value raw-tweet :user :lang))
      (setf (listed-count user) 
            (dx-utils:get-json-value raw-tweet :user :listed--count))
      (setf (location user) 
            (dx-utils:get-json-value raw-tweet :user :location))
      (setf (name user) 
            (dx-utils:get-json-value raw-tweet :user :name))
      (setf (profile-background-colour user) 
            (dx-utils:get-json-value raw-tweet :user :profile--background--colour))
      (setf (profile-background-image-url user) 
            (dx-utils:get-json-value raw-tweet :user :profile--background--image--url ))
      (setf (profile-background-image-url-https user) 
            (dx-utils:get-json-value raw-tweet :user :profile--background--image--url--https))
      (setf (profile-background-title user) 
            (dx-utils:get-json-value raw-tweet :user :profile--background--title))
      (setf (profile-banner-url user) 
            (dx-utils:get-json-value raw-tweet :user :profile--banner--url))
      (setf (profile-image-url user) 
            (dx-utils:get-json-value raw-tweet :user :profile--image--url))
      (setf (profile-image-url-https user) 
            (dx-utils:get-json-value raw-tweet :user :profile-image--url--https))
      (setf (profile-link-colour user) 
            (dx-utils:get-json-value raw-tweet :user :profile--link--colour))
      (setf (profile-sidebar-border-colour user) 
            (dx-utils:get-json-value raw-tweet :user :profile--sidebar--border--colour))
      (setf (profile-sidebar-fill-colour user) 
            (dx-utils:get-json-value raw-tweet :user :profile--sidebar--fill--colour))
      (setf (profile-text-colour user) 
            (dx-utils:get-json-value raw-tweet :user :profile--text--colour))
      (setf (profile-use-background-image user) 
            (dx-utils:get-json-value raw-tweet :user :profile--use--background--image))
      (setf (protected user) 
            (dx-utils:get-json-value raw-tweet :user :protected))
      (setf (screen-name user) 
            (dx-utils:get-json-value raw-tweet :user :screen--name))
      (if  (dx-utils:get-json-value raw-tweet :user :status)
           (setf (status user) 
                 (parse-tweet (or (tweet-exists raw-tweet) (make-instance 'tweet)) 
                              (dx-utils:get-json-value raw-tweet :user :status)))
           (setf (status user) nil))
      (setf (statuses-count user) 
            (dx-utils:get-json-value raw-tweet :user :statuses--count))
      (setf (twitter-user-time-zone user) 
            (dx-utils:get-json-value raw-tweet :user :time--zone))
      (setf (url user) 
            (dx-utils:get-json-value raw-tweet :user :url))
      (setf (utc-offset user) 
            (dx-utils:get-json-value raw-tweet :user :utc--offset))
      (setf (verified user) 
            (dx-utils:get-json-value raw-tweet :user :verified))
      (setf (witheld-in-countries user) 
            (dx-utils:get-json-value raw-tweet :user :witheld--in--countries))
      (setf (witheld-scope user) 
            (dx-utils:get-json-value raw-tweet :user :witheld--scope))
      user)))
 
(defmethod parse-place (raw-tweet)
  (when (dx-utils:get-json-value raw-tweet :place)
    (make-instance 
     'place
     :id (dx-utils:get-json-value raw-tweet :place :id)
     ;;Attributes can be user defined so not parsing it to an object
     :attributes (dx-utils:get-json-value raw-tweet :place :attributes) 
     :bounding-box (parse-coordinates 
                    (dx-utils:get-json-value raw-tweet :place :bounding--box))
     :country (dx-utils:get-json-value raw-tweet :place :country)
     :country-code (dx-utils:get-json-value raw-tweet :place :country--code)
     :full-name (dx-utils:get-json-value raw-tweet :place :full--name)
     :name (dx-utils:get-json-value raw-tweet :place :name)
     :place-type (dx-utils:get-json-value raw-tweet :place :place--type)
     :url (dx-utils:get-json-value raw-tweet :place :url))))

(defmethod parse-tweet ((tweet tweet) raw-tweet)

;;(break "~A" (dx-utils:get-json-value raw-tweet :id))

  (setf (id tweet) 
        (dx-utils:get-json-value raw-tweet :id))
  (setf (annotations tweet) 
        (dx-utils:get-json-value raw-tweet :annotations))
  (setf (contributors tweet) 
        (parse-contributors raw-tweet))
  (setf (coordinates tweet) 
        (parse-coordinates 
         (dx-utils:get-json-value raw-tweet :coordinates)))

  (setf (created-at tweet) 
        (parse-twitter-created-at 
         (dx-utils:get-json-value raw-tweet :created--at)))
  (setf (current-user-retweet-id tweet) 
        (dx-utils:get-json-value raw-tweet :current--user--retweet :id))
  (setf (tweet-entities tweet) 
        (parse-entities raw-tweet))
  (setf (favourite-count tweet) 
        (dx-utils:get-json-value raw-tweet :favourite--count))
  (setf (favourited tweet) 
        (dx-utils:get-json-value raw-tweet :favourited))
  (setf (filter-level tweet) 
        (dx-utils:get-json-value raw-tweet :filter--level))
  (setf (in-reply-to-screen-name tweet) 
        (dx-utils:get-json-value raw-tweet :in--relpy--to--screen--name))
  (setf (in-reply-to-status-id tweet) 
        (dx-utils:get-json-value raw-tweet :in--relpy--status--id))
  (setf (in-reply-to-user-id tweet) 
        (dx-utils:get-json-value raw-tweet :in--relpy--user--id))
  (setf (lang tweet) 
        (dx-utils:get-json-value raw-tweet :lang))
  (setf (place tweet) 
        (parse-place raw-tweet))
  (setf (possibly-sensitive tweet) 
        (dx-utils:get-json-value raw-tweet :possibly--sensitive))
  (setf (scopes tweet) 
        (dx-utils:get-json-value raw-tweet :scopes))
  (setf (retweet-count tweet) 
        (dx-utils:get-json-value raw-tweet :retweet--count))
  (setf (retweeted tweet) 
        (dx-utils:get-json-value raw-tweet :retweeted))
  (setf (source tweet) 
        (dx-utils:get-json-value raw-tweet :source))
  (setf (text tweet) 
        (dx-utils:get-json-value raw-tweet :text))
  (setf (truncated tweet) 
        (dx-utils:get-json-value raw-tweet :truncated))
  (setf (user tweet) 
        (parse-user raw-tweet))
  (setf (withheld-copyright tweet) 
        (dx-utils:get-json-value raw-tweet :withheld--copyright))
  (setf (withheld-in-countries tweet) 
        (dx-utils:get-json-value raw-tweet :withheld--in--countries))
  (setf (withheld-scope tweet) 
        (dx-utils:get-json-value raw-tweet :withheld--scope))
  tweet)
  
 
(defgeneric parse-tweets (tweets &key tweet-class))

(defmethod parse-tweets ((tweets list) &key (tweet-class 'tweet) )
  (if (not (dx-utils:get-json-value tweets :errors))
      (let ((parsed-tweets))
        (dolist (tweet tweets)
          (setf parsed-tweets 
                    (append parsed-tweets 
                            (list (parse-tweet (or (tweet-exists tweet)
                                                   (make-instance tweet-class)) 
                                               tweet)))))
        parsed-tweets)
    (dx-utils:get-json-value :errors :message)))